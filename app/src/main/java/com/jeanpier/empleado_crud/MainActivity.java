package com.jeanpier.empleado_crud;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.jeanpier.empleado_crud.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private ActivityMainBinding binding;
    private EditText editCedula;
    private EditText editFirstName;
    private EditText editLastName;
    private EditText editSalary;
    private Spinner spinType;
    private Spinner spinCity;
    private Spinner spinRole;
    private EditText editIess;
    private Button buttonCreate;
    private Button buttonRead;
    private Button buttonUpdate;
    private Button buttonDelete;

    private AdminSqliteOpenHelper admin;

    private final static String DB_NAME = "myBase";
    private final static String TABLE_NAME = "employers";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        admin = new AdminSqliteOpenHelper(this, DB_NAME, null, 1);

        editCedula = binding.textCedula;
        editFirstName = binding.textFirstname;
        editLastName = binding.textLastname;
        editSalary = binding.textCleanSalary;
        spinType = binding.spinnerEmployerType;
        spinCity = binding.spinnerCity;
        spinRole = binding.spinnerEmployerRole;
        editIess = binding.textIess;
        buttonCreate = binding.buttonCreate;
        buttonRead = binding.buttonSearch;
        buttonUpdate = binding.buttonUpdate;
        buttonDelete = binding.buttonDelete;

        buttonCreate.setOnClickListener(this::create);
        buttonRead.setOnClickListener(this::read);
        buttonUpdate.setOnClickListener(this::update);
        buttonDelete.setOnClickListener(this::delete);

//        actualiza el valor del salario neto en caso de ser gerente
        spinRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String itemSelected = parent.getItemAtPosition(position).toString();
                if (itemSelected.equals("Gerente")) {
                    Log.d(TAG, "onItemSelected: se seleccionó gerente");
                    editSalary.setText(R.string.gerente_salary);
                    editSalary.setEnabled(false);
                } else {
                    editSalary.setText("");
                    editSalary.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

//        actualiza dinámicamente el valor de aportación al IESS
        editSalary.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    editIess.setText("");
                    return;
                }
                Log.d(TAG, "afterTextChanged: " + s);
                float iessValue = Float.parseFloat(s.toString()) * 0.0945f;
                editIess.setText(String.valueOf(iessValue));
            }
        });
    }

    public void create(View view) {
        SQLiteDatabase database = admin.getWritableDatabase();

        String cedula = editCedula.getText().toString();
        String firstname = editFirstName.getText().toString();
        String lastname = editLastName.getText().toString();
        Float salary = Float.parseFloat(editSalary.getText().toString());
        String type = spinType.getSelectedItem().toString();
        String city = spinCity.getSelectedItem().toString();
        String role = spinRole.getSelectedItem().toString();
        Float iess = Float.parseFloat(editIess.getText().toString());

        if (cedula.isEmpty() || firstname.isEmpty() || lastname.isEmpty() ||
                type.isEmpty() || city.isEmpty() || role.isEmpty()) {
            showToast("Debe ingresar todos los campos");
        }

        ContentValues values = new ContentValues();
        values.put("cedula", cedula);
        values.put("firstname", firstname);
        values.put("lastname", lastname);
        values.put("salary", salary);
        values.put("type", type);
        values.put("city", city);
        values.put("role", role);
        values.put("iess", iess);

        try {
            database.insert(TABLE_NAME, null, values);
            clearAllEdits();
        } catch (SQLiteConstraintException e) {
            showToast("Ha ocurrido un eror. ¿El empleado que ingresó ya existe?");
        }
        database.close();
        showToast("Registro de empleado guardado con exito");
    }

    public void read(View view) {
        SQLiteDatabase database = admin.getReadableDatabase();
        String cedula = editCedula.getText().toString();
        if (cedula.isEmpty()) {
            showToast("Ingrese la cédula del empleado a buscar");
            return;
        }
        String query = "select name, salary from customers where cedula='" + cedula + "'";
        Cursor cursor = database.rawQuery(query, null);
        if (!cursor.moveToFirst()) {
            showToast("No se encontró al empleado a buscar");
            database.close();
            return;
        }
        editFirstName.setText(cursor.getString(0));
        editLastName.setText(cursor.getString(1));
        editSalary.setText(String.valueOf(cursor.getFloat(2)));
        String type = cursor.getString(3);
        String city = cursor.getString(4);
        String role = cursor.getString(5);
        spinType.setSelection(((ArrayAdapter) spinType.getAdapter()).getPosition(type));
        spinCity.setSelection(((ArrayAdapter) spinCity.getAdapter()).getPosition(city));
        spinRole.setSelection(((ArrayAdapter) spinRole.getAdapter()).getPosition(role));
        editIess.setText(String.valueOf(cursor.getFloat(6)));
        database.close();
        cursor.close();
    }

    public void update(View view) {
        SQLiteDatabase database = admin.getWritableDatabase();

        String cedula = editCedula.getText().toString();
        String firstname = editFirstName.getText().toString();
        String lastname = editLastName.getText().toString();
        Float salary = Float.parseFloat(editSalary.getText().toString());
        String type = spinType.getSelectedItem().toString();
        String city = spinCity.getSelectedItem().toString();
        String role = spinRole.getSelectedItem().toString();
        Float iess = Float.parseFloat(editIess.getText().toString());

        if (cedula.isEmpty() || firstname.isEmpty() || lastname.isEmpty() ||
                type.isEmpty() || city.isEmpty() || role.isEmpty()) {
            showToast("Debe ingresar todos los campos");
        }
        ContentValues values = new ContentValues();
        values.put("cedula", cedula);
        values.put("firstname", firstname);
        values.put("lastname", lastname);
        values.put("salary", salary);
        values.put("type", type);
        values.put("city", city);
        values.put("role", role);
        values.put("iess", iess);

        int nEmployersUpdated = database.update(TABLE_NAME, values, "cedula=" + cedula, null);
        database.close();

        if (nEmployersUpdated > 0) {
            showToast("Empleado actualizado");
            clearAllEdits();
        } else {
            showToast("Empleado no existe");
        }
    }

    public void delete(View view) {
        SQLiteDatabase database = admin.getWritableDatabase();
        String cedula = editCedula.getText().toString();

        if (cedula.isEmpty()) {
            showToast("Ingrese la cédula del empleado");
            database.close();
            return;
        }

        int nEmployersDeleted = database.delete(TABLE_NAME, "cedula=" + cedula, null);
        database.close();

        if (nEmployersDeleted > 0) {
            showToast("Eliminado con éxito");
            clearAllEdits();
        } else {
            showToast("El cliente no existe");
        }
    }

    private void clearAllEdits() {
        editCedula.setText("");
        editFirstName.setText("");
        editLastName.setText("");
        editSalary.setText("");
        editIess.setText("");
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}